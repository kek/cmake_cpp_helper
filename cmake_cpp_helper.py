import sys, os, argparse
from string import Template

cmake_ast_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "cmake-ast")
sys.path.append(cmake_ast_path)

from cmakeast import ast, ast_visitor

import argparse
import os

class CMakeAddClassHelperSettings:
    """
        Settings that can be passed to a AddClass command
    """
    def __init__(self):
        self.src_dir = 'src'
        self.include_dir = 'include'
        self.src_list = 'sources'
        self.include_list = 'includes'
        self.skip_include = False
        self.target_definition = None
        self.root_namespace = ''
        self.header_path = ''
        self.source_path = ''
        self.namespace = None

        self.parameters = {
            'src_dir' : ['-s', '--src-dir', 'Set source sub directory. Default:\'src\'', 'store'],
            'include_dir' : ['-i', '--include-dir', 'Set include sub directory. Default:\'include\'', 'store'],
            'src_list' : ['-S', '--src-list', 'Set cmake source list name. Default:\'sources\'', 'store'],
            'include_list' : ['-I', '--include-list', 'Set include sub directory. Default:\'include\'', 'store'],
            'target_definition' : ['-t', '--target-definition', 'Set target definition function. Default:\'add_exeutable\' or \'add_library\'', 'store'],
            'skip_include' : ['-k', '--skip-include', 'Do not add include in CMakeLists.txt', 'store_true'],
            'root_namespace' : ['-r', '--root-namespace', 'Sets the namespace in which to store the class (a sub namespace equal to the project name will be added anyway). Can be composed of multi namespaces with :: separator', 'store'],
            'header_path' : ['-H', '--header-path', 'Set sub path to header file. Default:projectName/classPath.h', 'store'],
            'source_path' : ['-C', '--source-path', 'Set sub path to source file. Default:className.cpp', 'store'],
            'namespace' : ['-n', '--namespace', 'Sets the relative namespace in which to store the class. Default is project name. Can be composed of multi namespaces with :: separator', 'store']
            }

    def add_arguments_to_parser(self, parser):
        """
        Fills a parser with all arguments that can be parsed by self.
        """
        for key, value in self.parameters.items():
            parser.add_argument(value[0], value[1], help=value[2], action=value[3])

    def set_arguments_values(self, args):
        """
        Reads values from argument parser and sets fields values
        """
        args_list = {k:v for k,v in args._get_kwargs() if v is not None}
        for key, value in self.parameters.items():
            if key in args_list:
                param_value = args_list[key]
                if param_value is not None:
                    setattr(self, key, param_value)
        
class CMakeFunctionCallAccessor:
    def __init__(self, name, line):
        self.name = name
        self.arguments = []
        self.first_line = line
        self.last_line = line
    
    def get_first_line(self):
        return self.first_line

    def get_last_line(self):
        return self.last_line
    
    def function_name(self):
        return self.name

    def insert_at_end(self, cmakelist_content, content_to_add):
        last_line = cmakelist_content[self.get_last_line() - 1]
        #what if ) is within " or something... Just ignore t until someone complains
        #If first line is not last line, would seem a gooe idea to insert a new line as well : set(sources "foo.cpp"\n "bar.cpp")
        if ')' in last_line:
            last_parenthesis_pos = last_line.rfind(')')
            last_line = '%s %s%s' % (last_line[:last_parenthesis_pos], content_to_add, last_line[last_parenthesis_pos:])
            cmakelist_content[self.get_last_line() - 1] = last_line
        else:
            cmakelist_content.insert(self.get_last_line(), '%s\n' % content_to_add)

    def move_down(self, idx = 1):
        self.first_line = self.first_line + idx
        self.last_line = self.last_line + idx

class CMakeFunctionCallNodeAccessor(CMakeFunctionCallAccessor):
    def __init__(self, node):
        super(CMakeFunctionCallNodeAccessor, self).__init__(node.name, node.line)
        arguments = node.arguments
        if isinstance(arguments, list):
            for arg in arguments:
                if isinstance(arg, ast.Word):
                    self.arguments.append(arg.contents)
                    if arg.line > self.last_line:
                        self.last_line = arg.line
                else:
                    self.arguments.append('')
        
    def parameter_string(self, position):
        return self.arguments[position]

class CMakeFunctionCallAdder(CMakeFunctionCallAccessor):
    """
    Behaves as a CMakeFunctionCallNodeAccessor, but adds content to the cmakelists

    For now, it considers there is nothing else on target_line_number that target definition itself
    It will actually add lines before target_line_number
    """
    def __init__(self, name, cmakelist_content, target_line_number, initial_content, ast):
        super(CMakeFunctionCallAdder, self).__init__(name, target_line_number)
        self.first_line = target_line_number
        self.last_line = target_line_number
        current_line = self.last_line
        for content_line in initial_content:
            cmakelist_content.insert(current_line - 1, '%s\n' % content_line)
            current_line = current_line + 1

class CMakeAddClass:
    def __init__(self, ast, cmakelists_path, project_name, class_name, settings):
        self.ast = ast
        last_slash = class_name.rfind('/')
        if last_slash == -1:
            self.class_name = class_name
            self.class_path_component = []
        else:
            self.class_name = class_name[last_slash+1:]
            self.class_path_component = class_name[:last_slash].split('/')
        self.cmakelists_path = cmakelists_path
        self.cmakelists_dir = os.path.dirname(self.cmakelists_path)
        self.get_cmakelists_lines(cmakelists_path)
        self.project_name = project_name
        self.settings = settings

    def get_cmakelists_lines(self, cmakelists_path):
        self.cmakelists_lines = []
        with open(cmakelists_path) as fp:
            line = fp.readline()
            while line:
                self.cmakelists_lines.append(line)
                line = fp.readline()

    def write_cmakelists_content(self):
        os.replace(self.cmakelists_path, '%s.bak' % self.cmakelists_path)
        with open(self.cmakelists_path, 'w') as fp:
            fp.writelines(self.cmakelists_lines)

    def check_function_call_is_add_set(self, name, node, depth, list_name):
        if self.found:
            return
        parser = CMakeFunctionCallNodeAccessor(node)
        if parser.function_name().lower() == 'set':
            if parser.parameter_string(0) == list_name:
                self.found = True
                self.list = parser

    def find_set_listener(self, list_name):
        self.found = False
        return lambda name, node, depth:self.check_function_call_is_add_set(name, node, depth, list_name)

    def find_set(self, list_name):
        ast_visitor.recurse(self.ast, function_call= self.find_set_listener(list_name))
        if self.found:
            return self.list
        return None

    def check_function_call_is_target(self, name, node, depth):
        if self.found:
            return
        parser = CMakeFunctionCallNodeAccessor(node)
        if self.settings.target_definition is not None:
            test_target = lambda f: f.lower() == self.settings.target_definition
        else:
            test_target = lambda f: f.lower() == 'add_executable' or f.lower() == 'add_library'
        if test_target(parser.function_name()):
            self.found = True
            self.target_definition = parser

    def find_target_listener(self):
        self.found = False
        return lambda name, node, depth:self.check_function_call_is_target(name, node, depth)

    def find_target_definition(self):
        ast_visitor.recurse(self.ast, function_call = self.find_target_listener())
        if self.found:
            return self.target_definition
        else:
            return None

    def add_source_file(self, set_source):
        set_source.insert_at_end(self.cmakelists_lines,  '"%s"' % self.get_src_file_name())
        
    def add_include_file(self, set_include):
        set_include.insert_at_end(self.cmakelists_lines, '"%s"' % self.get_include_file_path())
        
    def get_namespace_header(self):
        result = '\n'
        if len(self.settings.root_namespace) > 0:
            namespace_components = self.settings.root_namespace.split('::')
            for component in namespace_components:
                result = '%snamespace %s\n{\n' % (result, component)
        if self.settings.namespace is None:
            if len(self.project_name) > 0:
                result = '%snamespace %s\n{\n' % (result, self.project_name)
            for component in self.class_path_component:
                result = '%snamespace %s\n{\n' % (result, component)
        elif len(self.settings.namespace) > 0:
            namespace_components = self.settings.namespace.split('::')
            for component in namespace_components:
                result = '%snamespace %s\n{\n' % (result, component)
        return result

    def get_namespace_footer(self):
        result = '\n'
        if len(self.settings.root_namespace) > 0:
            namespace_components = self.settings.root_namespace.split('::')
            for component in namespace_components:
                result = '%s}\n' % result
        if self.settings.namespace is None:
            if len(self.project_name) > 0:
                result = '%s}\n' % result
            for component in self.class_path_component:
                result = '%s}\n' % result
        elif len(self.settings.namespace) > 0:
            namespace_components = self.settings.namespace.split('::')
            for component in namespace_components:
                result = '%s}\n' % result
        return result

    def get_include_dir(self):
        return self.settings.include_dir

    def get_src_dir(self):
        return self.settings.src_dir

    def get_include_sub_dir(self):
        if len(self.class_path_component) > 0:
            return '%s/%s' % (self.project_name, '/'.join(self.class_path_component))
        else:
            return self.project_name

    def get_include_relative_file_path(self):
        result = ''
        if len(self.settings.header_path) > 0:
            result = self.settings.header_path
        elif len(self.get_include_sub_dir()) > 0:
            result = '%s/%s.h' % (self.get_include_sub_dir(), self.class_name)
        else:
            result = '%s.h' % self.class_name
        return result

    def get_include_file_path(self):
        return '%s/%s' %(self.get_include_dir(), self.get_include_relative_file_path())

    def get_src_file_name(self):
        if len(self.settings.source_path) > 0:
            return '%s/%s' % (self.get_src_dir(), self.settings.source_path)
        return '%s/%s.cpp' % (self.get_src_dir(), self.class_name)

    def create_include_template_dir(self):
        return  {'namespace': self.get_namespace_header(),'namespace_close': self.get_namespace_footer(), 'class_name':self.class_name, 'indent':'\t'}

    def create_source_template_dir(self):
        return {'namespace': self.get_namespace_header(),'namespace_close': self.get_namespace_footer(), 'class_name':self.class_name, 'include_path': '<%s>' % self.get_include_relative_file_path()}
    
    def create_source_file(self):
        d = self.create_source_template_dir()
        with open( os.path.join(os.path.dirname(os.path.realpath(__file__)),'src-file.tpl') ) as template_file:
            tpl = Template( template_file.read() )
            src_path = os.path.join(self.cmakelists_dir, self.get_src_file_name())
            os.makedirs(os.path.dirname(src_path), exist_ok=True)
            with open(src_path,"w") as src_file:
                src_file.write(tpl.substitute(d))
        return src_path

    def create_include_file(self):
        d = self.create_include_template_dir()
        with open( os.path.join(os.path.dirname(os.path.realpath(__file__)),'include-file.tpl') ) as template_file:
            tpl = Template( template_file.read() )
            header_path = os.path.join(self.cmakelists_dir, self.get_include_file_path())
            os.makedirs(os.path.dirname(header_path), exist_ok=True)
            with open(header_path,"w+") as include_file:
                include_file.write(tpl.substitute(d))
        return header_path

    def run(self):
        target_definition = self.find_target_definition()
        if target_definition is None:
            raise RuntimeError('Cannot find target definition in CMakeLists.txt. Are you actually building something?')
        set_sources = self.find_set(self.settings.src_list)
        if set_sources is None:
            #Todo add setting to know if we should raise or create list
            #raise RuntimeError('Cannot find list %s' % self.settings.src_list)
            set_sources = CMakeFunctionCallAdder('set', self.cmakelists_lines, target_definition.get_first_line(), ['set(%s' % self.settings.src_list,')', '' ], self.ast)
            target_definition.move_down(3)
            target_definition.insert_at_end(self.cmakelists_lines, '${%s}' % self.settings.src_list)

        if not self.settings.skip_include:
            set_includes = self.find_set(self.settings.include_list)
            if set_includes is None:
                #Todo add setting to know if we should raise or create list
                #raise RuntimeError('Cannot find list %s' % self.settings.include_list)
                set_includes = CMakeFunctionCallAdder('set', self.cmakelists_lines, target_definition.get_first_line(), ['set(%s' % self.settings.include_list,')', '' ], self.ast)
                target_definition.move_down(3)
                target_definition.insert_at_end(self.cmakelists_lines, '${%s}' % self.settings.include_list)
        
        source_file_path = self.create_source_file()
        header_file_path = self.create_include_file()
            
        if self.settings.skip_include:
            self.add_source_file(set_sources)
        else:
            if set_sources.get_last_line() > set_includes.get_last_line():
                self.add_source_file(set_sources)
                self.add_include_file(set_includes)
            else:
                self.add_include_file(set_includes)
                self.add_source_file(set_sources)
        self.write_cmakelists_content()
        return header_file_path, source_file_path

class CMakeHelper:
    def __init__(self):
        pass

    def get_ast(self, path):
        data = ''
        with open(path,'r') as file:
            data = file.read()
        return ast.parse(data)

    def get_function_call_subdirectory(self, name, node, depth):
        parser = CMakeFunctionCallNodeAccessor(node)
        if parser.function_name() == 'add_subdirectory':
            self.sub_directories.append(parser.parameter_string(0))

    def get_all_add_subdirectory_helper(self, sub_directory):
        self.sub_directories = []
        return lambda name, node, depth:self.get_function_call_subdirectory(name, node, depth)

    def get_all_add_subdirectory(self, list_directory):
        cmakelist_path = os.path.join(list_directory, 'CMakeLists.txt')
        if not os.path.isfile(cmakelist_path):
            raise Exception('No CMakeFile found in %s' % list_directory)
        current_ast = self.get_ast(cmakelist_path)
        ast_visitor.recurse(current_ast, function_call= self.get_all_add_subdirectory_helper(list_directory))
        return self.sub_directories

    def extract_possible_projects_path_rec(self, project_path, paths, result):
        result.append([project_path, '/'.join(paths)])
        if len(paths) == 1:
            return
        else:
            sub_dir = self.get_all_add_subdirectory(project_path)
            for i in range(1,len(paths)):
                sub_project_path = '/'.join(paths[:i])
                if(sub_project_path in sub_dir):
                   self.extract_possible_projects_path_rec(os.path.join(project_path, sub_project_path), paths[i:], result)
                
    def extract_possible_projects_path(self, class_name):
        paths = class_name.split('/')
        project_path = '.'
        result = []
        if not os.path.isfile('CMakeLists.txt'):
            raise Exception('No CMakeFile found')
        self.extract_possible_projects_path_rec(project_path, paths, result)
        return result

    def extract_project_path(self, class_name):
        all_paths = self.extract_possible_projects_path(class_name)
        result = all_paths[0]
        for path in all_paths:
            if len(path[0]) > len(result):
                result = path
        return result[0], result[1]

    def add_class(self, class_name, settings):
        project_path, class_path = self.extract_project_path(class_name)
        project_name = project_path[project_path.rfind(os.sep) +1:]
        project_name = project_name[project_name.rfind('/') +1:]
        cmakelists_path = os.path.join(project_path, 'CMakeLists.txt')
        add_class_helper = CMakeAddClass(self.get_ast(cmakelists_path), cmakelists_path, project_name, class_path, settings)
        header_file_path, source_file_path = add_class_helper.run()
        print('added %s and %s' % (header_file_path, source_file_path))

def main():
    parser = argparse.ArgumentParser(description='Help CMake projects to create classes..')
    subparsers = parser.add_subparsers(help='Different possible commands enter a command and -h to have more information')
    
    parser_add_class = subparsers.add_parser('add-class')
    parser_add_class.add_argument('add_class', metavar='add-class', help='Adds a class to project or subproject')

    add_class_settings = CMakeAddClassHelperSettings()
    add_class_settings.add_arguments_to_parser(parser_add_class)

    parser_remove_class = subparsers.add_parser('remove-class')
    parser_remove_class.add_argument('remove_class', metavar='remove_class', help='Removes a class to project or subproject')

    args = parser.parse_args()

    cmakeHelper = CMakeHelper()
    if hasattr(args, 'add_class'):
        add_class_settings.set_arguments_values(args)
        cmakeHelper.add_class(args.add_class, add_class_settings)

if __name__ == "__main__":
    main()

