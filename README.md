# CMAKE C++ HELPER

CMake C++ Helper is a set of tools that ease C++ project creation & modification using CMake.
Adding a class in a C++ project should be as easy as possible; but doing so in a CMake project is quite painful!
CMake C++ Helper simplify the CMakeLists.txt modification for day to day tasks.

## Getting started

CMake C++ Helper is based on cmaks-ast project (https://github.com/polysquare/cmake-ast) an includes it as a git submodule. So doing a simple `git submodule update`should be enough. If you appear not be using git, well, simply create a cmake-ast directory and copy the content of cmake-ast repository in it.

The main entry point of CMake C++ Helper is a python file. So well, you'll have to install python 3 as well.

Then run `python cmake_cpp_helper.py --help` for a list of commands.

For now, only class addition is supported:

`python cmake_cpp_helper.py add-class sub/directory/ClassName`

This will add 2 files ClassName.cpp & ClassName.h in your project and the right CMakeLists.txt file

There a a number of parameter you can pass to select whether or not add the header file to the CMakeLists.txt, choose source & include folders or names of lists in which to add the files in the CMakeLists.txt file.

## Visual Studio Extension

CMake C++ Helper also comes as a Visual studio extension in cmake_cpp_helper_ext directory.
This extension simply adds an entry "Add class in cmake project..." in Add context menu on a project.
This will automatically modify your CMakeLists.txt and rerun compilation so that you class files appear immediately.

For the extension to work, Python 3 must be installed and present in the path
