﻿using Microsoft.VisualStudio.PlatformUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cmake_cpp_helper_ext
{
    /// <summary>
    /// Logique d'interaction pour AddClassWindow.xaml
    /// </summary>
    public partial class AddClassWindow : UserControl
    {
        public AddClassWindow(AddClassDialog parent, string relativeDir, ScriptRunner scriptRunner)
        {
            InitializeComponent();
            this.parent = parent;
            this.relativeDir = relativeDir;
            this.scriptRunner = scriptRunner;
            relNamespace.Default = relativeDir;
            includePath.Default = "include/" + relativeDir + "/ClassName.h";
            projectName = relativeDir;
        }

        private string buildCommand()
        {
            var allTextParameters = new Dictionary<string, string>
            {
                { " -t ",  targetDef.IsDefault ? null : targetDef.Value},
                { " -C ",  srcPath.IsDefault ? null : srcPath.Value},
                { " -H ",  includePath.IsDefault ? null : includePath.Value},
                { " -s ",  srcDir.IsDefault ? null : srcDir.Value},
                { " -i ",  includeDir.IsDefault ? null : includeDir.Value},
                { " -S ",  srcList.IsDefault ? null : srcList.Value},
                { " -I ",  chkGenerateInclude.IsChecked == true && includeList.IsDefault ? null : includeList.Value},
                { " -r ",  rootNamepace.IsDefault ? null: rootNamepace.Value},
                { " -n ",  relNamespace.IsDefault ? null: relNamespace.Value}
            };
            var classFullName = relativeDir + '/' + className.Text;
            var result = new System.Text.StringBuilder();
            result.Append("add-class ");
            result.Append(classFullName);
            foreach (var param in allTextParameters)
            {
                if (!string.IsNullOrEmpty(param.Value))
                {
                    result.Append(param.Key);
                    result.Append(param.Value);
                }
            }
            if (!relNamespace.IsDefault && relNamespace.Value == "")
                result.Append(" -n \"\"");
            if(chkGenerateInclude.IsChecked != true)
                result.Append(" -k");
            return result.ToString();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            var classFullName = relativeDir + '/' + className.Text;
            try
            {
                var res = scriptRunner.runCommand(buildCommand()).TrimEnd(new char[] { '\r', '\n' });
                parent.DialogResult = true;

                var regex = new Regex("added (.*) and (.*)");
                Match match = regex.Match(res);
                if (match.Length > 2)
                {
                    parent.HeaderFilePath = match.Groups[1].Value;
                    parent.SourceFilePath = match.Groups[2].Value;
                }
                parent.BuildCMake = chk_build_cmake.IsChecked == true;
                parent.Close();
            }
            catch(ScriptRunningException ex)
            {
                MessageBox.Show(ex.Message, "An error occured");
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            parent.DialogResult = false;
            parent.Close();
        }

        private AddClassDialog parent;
        private string relativeDir;
        private ScriptRunner scriptRunner;

        private void className_TextChanged(object sender, TextChangedEventArgs e)
        {
            var components = className.Text.Split(new char[]{ '/' });
            srcPath.Default = components[components.Length -1] + ".cpp";
            if(string.IsNullOrEmpty(projectName))
                includePath.Default = className.Text + ".h";
            else
                includePath.Default = projectName + '/' + className.Text + ".h";
        }

        private string projectName;
    }
}
