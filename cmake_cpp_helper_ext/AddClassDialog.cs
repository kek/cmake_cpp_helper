﻿using Microsoft.VisualStudio.PlatformUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cmake_cpp_helper_ext
{
    public class AddClassDialog: DialogWindow
    {
        public AddClassDialog(string relativeDir, ScriptRunner scriptRunner)
        {
            var window = new AddClassWindow(this, relativeDir, scriptRunner);
            Content = window;
            window.Focus();            
        }

        public string HeaderFilePath { get; set; }
        public string SourceFilePath { get; set; }

        public bool BuildCMake { get; set; }
    }
}
