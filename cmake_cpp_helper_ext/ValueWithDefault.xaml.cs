﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cmake_cpp_helper_ext
{
    /// <summary>
    /// Logique d'interaction pour ValueWithDefault.xaml
    /// </summary>
    public partial class ValueWithDefault : UserControl
    {
        public event EventHandler ValueChanged;

        //TODO: use DependencyProperty  instead of simple properties
        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register("Label", typeof(string), typeof(ValueWithDefault), new PropertyMetadata(""));
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(ValueWithDefault), new PropertyMetadata("", OnValuePropertyChanged, OnCoerceValue));
        public static readonly DependencyProperty DefaultProperty = DependencyProperty.Register("Default", typeof(string), typeof(ValueWithDefault), new PropertyMetadata("", OnIsDefaultPropertyChanged));
        public static readonly DependencyProperty IsDefaultProperty = DependencyProperty.Register("IsDefault", typeof(bool), typeof(ValueWithDefault), new PropertyMetadata(true, OnIsDefaultPropertyChanged));
        //public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(ValueWithDefault), new PropertyMetadata(true));

        private static void OnValuePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var vd = (ValueWithDefault)d;
            if(vd.ValueChanged != null)
                vd.ValueChanged(vd, new EventArgs());
        }

        private static void OnIsDefaultPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var vd = (ValueWithDefault)d;
            if (e.NewValue is bool)
            {
                if ((bool)e.NewValue)
                    vd.CoerceValue(ValueProperty);
            }
            else if (vd.IsDefault)
                vd.CoerceValue(ValueProperty);
        }

        private static object OnCoerceValue(DependencyObject d, object baseValue)
        {
            var vd = (ValueWithDefault)d;
            var isDefault = vd.IsDefault;
            return ((isDefault) ? vd.Default : (string)baseValue);
        }

        public ValueWithDefault()
        {
            IsDefault = true;
            InitializeComponent();
        }

        public string Label
        {
            get { return (string)this.GetValue(LabelProperty); }
            set { this.SetValue(LabelProperty, value); }
        }

        public string Value
        {
            get { return (string)this.GetValue(ValueProperty); }
            set { this.SetValue(ValueProperty, value); }
        }

        public bool IsDefault
        {
            get { return (Boolean)this.GetValue(IsDefaultProperty); }
            set { this.SetValue(IsDefaultProperty, value); }
        }

        public string Default
        {
            get { return (string)this.GetValue(DefaultProperty); }
            set { this.SetValue(DefaultProperty, value); }
        }

        private void txtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (((TextBox)sender).IsFocused && IsDefault)
                IsDefault = false;
        }

        private void txtBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (IsDefault)
            {
                var textbox = (sender as TextBox);
                if (textbox != null && !textbox.IsKeyboardFocusWithin)
                {
                    if (e.OriginalSource.GetType().Name == "TextBoxView")
                    {
                        e.Handled = true;
                        textbox.Focus();
                    }
                }
            }
        }

        private void txtBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (IsDefault)
                ((TextBox)sender).SelectAll();
        }
    }
}
