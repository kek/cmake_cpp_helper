﻿using System;
using System.ComponentModel.Design;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Task = System.Threading.Tasks.Task;

namespace cmake_cpp_helper_ext
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class AddClassCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("351c435f-28f6-4891-849f-d2c8314f55ef");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly AsyncPackage package;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddClassCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private AddClassCommand(AsyncPackage package, OleMenuCommandService commandService)
        {
            this.package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new MenuCommand(this.Execute, menuCommandID);
            commandService.AddCommand(menuItem);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static AddClassCommand Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(AsyncPackage package)
        {
            // Switch to the main thread - the call to AddCommand in AddClassCommand constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            OleMenuCommandService commandService = await package.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Instance = new AddClassCommand(package, commandService);
        }

        internal static Project GetActiveProject(DTE dte)
        {
            Project activeProject = null;

            Array activeSolutionProjects = dte.ActiveSolutionProjects as Array;
            if (activeSolutionProjects != null && activeSolutionProjects.Length > 0)
            {
                activeProject = activeSolutionProjects.GetValue(0) as Project;
            }

            return activeProject;
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            // Show a message box to prove we were here
            
            DTE dte = Package.GetGlobalService(typeof(SDTE)) as DTE;
            
            var project = GetActiveProject(dte);
            var solution = dte.Solution;

            var projectDir = System.IO.Path.GetDirectoryName(project.FullName);
            var solutionDir = System.IO.Path.GetDirectoryName(solution.FullName);
            if (!projectDir.StartsWith(solutionDir))
                throw new ArgumentException();
            var relativeDir = projectDir.Substring(solutionDir.Length + 1).Replace('\\', '/');
            string rootCMakeListsPath = null;
            EnvDTE.Project zeroCheck = null;
            
            for (int i = 1; i <= solution.Projects.Count; i++)
            {
                var proj = solution.Projects.Item(i);
                if (proj.Name == "CMakePredefinedTargets")
                {
                    for (int j = 1; j <= proj.ProjectItems.Count; ++j)
                    {
                        var item = proj.ProjectItems.Item(j);
                        if (item.Object is EnvDTE.Project)
                        {
                            var subProj = (EnvDTE.Project)item.Object;
                            if (subProj.Name == "ALL_BUILD")
                            {
                                rootCMakeListsPath = getCMakePath(subProj.ProjectItems);
                            }
                            else if(subProj.Name == "ZERO_CHECK")
                            {
                                zeroCheck = subProj;
                            }
                        }
                    }
                }
                else if (proj.Name == "ALL_BUILD")
                {
                    rootCMakeListsPath = getCMakePath(proj.ProjectItems);
                }
                else if (proj.Name == "ZERO_CHECK")
                {
                    zeroCheck = proj;
                }
            }
            if (string.IsNullOrEmpty(rootCMakeListsPath))
                throw new ArgumentException();

            AddClassDialog testDialog = new AddClassDialog(relativeDir, new ScriptRunner(System.IO.Path.GetDirectoryName(rootCMakeListsPath)));
            var shouldReRunCMake = testDialog.ShowDialog();
            if(zeroCheck != null && shouldReRunCMake.HasValue && shouldReRunCMake.Value)
            {
                if (testDialog.BuildCMake)
                {
                    //Re run zero check compilation
                    var solutionBuilder = dte.Solution.SolutionBuild;

                    //zeroCheck.ConfigurationManager.ActiveConfiguration.
                    var configuration = zeroCheck.ConfigurationManager.ActiveConfiguration.ConfigurationName;
                    configuration += "|" + zeroCheck.ConfigurationManager.ActiveConfiguration.PlatformName;
                    solutionBuilder.BuildProject(zeroCheck.ConfigurationManager.ActiveConfiguration.ConfigurationName, zeroCheck.UniqueName, true);
                }

                var fullHeaderPath = Path.GetFullPath(Path.Combine(System.IO.Path.GetDirectoryName(rootCMakeListsPath), testDialog.HeaderFilePath));
                var fullSourcePath = Path.GetFullPath(Path.Combine(System.IO.Path.GetDirectoryName(rootCMakeListsPath), testDialog.SourceFilePath));
                dte.ItemOperations.OpenFile(fullHeaderPath);
                dte.ItemOperations.OpenFile(fullSourcePath);
            }
        }

        private static string getCMakePath(ProjectItems projItems)
        {
            string rootCMakeListsPath = null;
            for (int j = 1; j <= projItems.Count; ++j)
            {
                var item = projItems.Item(j);
                if (item.Name.ToLower() == "cmakelists.txt")
                {
                    rootCMakeListsPath = item.FileNames[0];
                    if (!string.IsNullOrEmpty(rootCMakeListsPath))
                        break;
                }
            }

            return rootCMakeListsPath;
        }
    }
}
